﻿using Newtonsoft.Json;

namespace LokumClient.Models
{
    public class RootObject
    {
        public int status { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public Results results { get; set; }
        public int count { get; set; }
        public int start { get; set; }
        public int end { get; set; }
    }

    public class Results
    {
        [JsonProperty("0")]
        public Flat _0 { get; set; }

        [JsonProperty("1")]
        public Flat _1 { get; set; }

        [JsonProperty("2")]
        public Flat _2 { get; set; }

        [JsonProperty("3")]
        public Flat _3 { get; set; }

        [JsonProperty("4")]
        public Flat _4 { get; set; }

        [JsonProperty("5")]
        public Flat _5 { get; set; }

        [JsonProperty("6")]
        public Flat _6 { get; set; }

        [JsonProperty("7")]
        public Flat _7 { get; set; }

        [JsonProperty("8")]
        public Flat _8 { get; set; }

        [JsonProperty("9")]
        public Flat _9 { get; set; }
    }

    public class Flat
    {
        public string vid { get; set; }
        public string uid { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public string nid { get; set; }
        public string created { get; set; }
        public string changed { get; set; }
        public Field_Ref_Inwestycja field_ref_inwestycja { get; set; }
        public Field_Building field_building { get; set; }
        public Field_Level field_level { get; set; }
        public Field_Yardage field_yardage { get; set; }
        public Field_Rooms field_rooms { get; set; }
        public Field_Price_Tax field_price_tax { get; set; }
        public Field_Price_Tax_M2 field_price_tax_m2 { get; set; }
        public Field_Status field_status { get; set; }
        public Field_Params field_params { get; set; }
        public Field_Area field_area { get; set; }
        public Field_World_Side field_world_side { get; set; }
        public Field_Stage field_stage { get; set; }
        public Labels labels { get; set; }
    }

    public class Field_Ref_Inwestycja
    {
        public Und und { get; set; }
    }

    public class Und
    {
        [JsonProperty("0")]
        public _01 _0 { get; set; }
    }

    public class _01
    {
        public string nid { get; set; }
        public Node node { get; set; }
    }

    public class Node
    {
        public string vid { get; set; }
        public string uid { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public string nid { get; set; }
        public Field_Investment_Type field_investment_type { get; set; }
        public Field_City field_city { get; set; }
        public Field_Street field_street { get; set; }
        public Field_City_Cd field_city_cd { get; set; }
        public Field_List_Label field_list_label { get; set; }
        public Field_Seo_Desc field_seo_desc { get; set; }
        public Field_Investment_Status field_investment_status { get; set; }
    }

    public class Field_Investment_Type
    {
        public Und1 und { get; set; }
    }

    public class Und1
    {
        [JsonProperty("0")]
        public _02 _0 { get; set; }
    }

    public class _02
    {
        public string tid { get; set; }
        public Taxonomy_Term taxonomy_term { get; set; }
    }

    public class Taxonomy_Term
    {
        public string tid { get; set; }
        public string vid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Field_Title field_title { get; set; }
    }

    public class Field_City
    {
        public Und1 und { get; set; }
    }

    public class Field_Street
    {
        public Und3 und { get; set; }
    }

    public class Und3
    {
        [JsonProperty("0")]
        public _04 _0 { get; set; }
    }

    public class _04
    {
        public string value { get; set; }
    }

    public class Field_City_Cd
    {
        public Und3 und { get; set; }
    }


    public class Field_List_Label
    {
        public Und3 und { get; set; }
    }


    public class Field_Seo_Desc
    {
        public Und1 und { get; set; }
    }

    public class Field_Title
    {
        public Und3 und { get; set; }
    }


    public class Field_Investment_Status
    {
        public Und1 und { get; set; }
    }

    public class Field_Building
    {
        public Und3 und { get; set; }
    }

    public class Field_Level
    {
        public Und3 und { get; set; }
    }


    public class Field_Yardage
    {
        public Und3 und { get; set; }
    }

    public class Field_Rooms
    {
        public Und3 und { get; set; }
    }

    public class Field_Price_Tax
    {
        public Und3 und { get; set; }
    }

    public class Field_Price_Tax_M2
    {
        public Und3 und { get; set; }
    }

    public class Field_Status
    {
        public Und1 und { get; set; }
    }

    public class Field_Params
    {
        public Und16 und { get; set; }
    }

    public class Und16
    {
        [JsonProperty("0")]
        public _02 _0 { get; set; }

        [JsonProperty("1")]
        public _02 _1 { get; set; }

        [JsonProperty("2")]
        public _02 _2 { get; set; }
    }

    public class Field_Area
    {
        public Und3 und { get; set; }
    }

    public class Field_World_Side
    {
        public Und1 und { get; set; }
    }

    public class Field_Stage
    {
        public Und1 und { get; set; }
    }

    public class Labels
    {
        [JsonProperty("0")]
        public _021 _0 { get; set; }
    }

    public class _021
    {
        public string title { get; set; }
    }
}