﻿using System;
using System.Windows.Input;
using LokumClient.Commands;
using LokumClient.Views;

namespace LokumClient.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IMainWindow _mainWindow;
        private DateTime _updateDateTime;
        private string _status;
        private int _flatsCount;

        private ICommand _showHideMainWindowCommand;
        private ICommand _openSiteCommand;
        private ICommand _updateInfoCommand;
        private ICommand _closeAppCommand;

        public MainViewModel(IMainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            _updateDateTime = DateTime.Now;
        }

        public DateTime UpdateDateTime
        {
            get { return _updateDateTime; }
            set
            {
                //_updateDateTime = value;
                //OnPropertyChanged();
                SetProperty(ref _updateDateTime, value);
            }
        }

        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        public int FlatsCount
        {
            get { return _flatsCount; }
            set { SetProperty(ref _flatsCount, value); }
        }

        public ICommand CloseAppCommand
        {
            get
            {
                return _closeAppCommand ?? (_closeAppCommand = new RelayCommand((o) => _mainWindow.CloseApp()));
            }
        }

        public ICommand OpenSiteCommand
        {
            get
            {
                return _openSiteCommand ?? (_openSiteCommand = new RelayCommand((o) => _mainWindow.OpenSite()));
            }
        }

        public ICommand ShowHideMainWindowCommand
        {
            get
            {
                return _showHideMainWindowCommand ??
                       (_showHideMainWindowCommand = new RelayCommand(o =>
                       {
                           var isForceShow = Convert.ToBoolean(o);
                           _mainWindow.ShowHideApp(isForceShow);
                       }));
            }
        }

        public ICommand UpdateInfoCommand
        {
            get
            {
                return _updateInfoCommand ?? (_updateInfoCommand = new RelayCommand((o) => _mainWindow.UpdateInfoAsync()));
            }
        }

    }
}