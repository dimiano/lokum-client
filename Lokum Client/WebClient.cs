﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using LokumClient.Models;
using Newtonsoft.Json;

namespace LokumClient
{
    public class WebClient
    {
        public static readonly string NavigationUrl = "http://lokum-deweloper.pl/mieszkania/lokum-da-vinci/#cennik";

        private static readonly string LokumDaVinciUrl = "http://lokum-deweloper.pl/locals_search/angular/?" +
                                                         "cities%5B%5D=3&investments%5B%5D=9&levels%5B%5D=1&levels%5B%5D=2&levels%5B%5D=3&levels%5B%5D=4&levels%5B%5D=5&levels%5B%5D=6&levels%5B%5D=7&levels%5B%5D=8&levels%5B%5D=9&levels%5B%5D=10&levels%5B%5D=11&levels%5B%5D=12&levels%5B%5D=13&levels%5B%5D=14&" +
                                                         "limit=10&locals_type%5B%5D=25&locals_type%5B%5D=63&page=1&" +
                                                         "room_max=3&room_min=3&sort=price_asc&stage%5B%5D=141&status=27&" +
                                                         "total_price_m2_max=6800&total_price_m2_min=5000&" +
                                                         "total_price_max=395000&total_price_min=330000&" +
                                                         "yardage_max=80&yardage_min=51";

        public async Task<RootObject> GetFlatInfoAsync()
        {
            var result = new RootObject
            {
                data = new Data { count = 0, end = 0, results = new Results(), start = 0 },
                status = 404
            };

            try
            {
                var http = new HttpClient();
                var response = await http.GetAsync(LokumDaVinciUrl);
                var resultText = await response.Content.ReadAsStringAsync();
                File.WriteAllText($"LOKUM_response_{DateTime.UtcNow:yyyy-MM-dd_HH-mm-ss}.LKM", resultText);

                //var result = File.ReadAllText("LOKUM_response.LKM");
                result = JsonConvert.DeserializeObject<RootObject>(resultText);

                return result;
            }
            catch (WebException webException)
            {
                result.status = (int) webException.Status;
                return result;
            }
            catch (Exception exception)
            {
                // Another file related exception
                return result;
            }
        }
    }
}