using System.Threading.Tasks;

namespace LokumClient.Views
{
    public interface IMainWindow
    {
        Task UpdateInfoAsync();
        void CloseApp();
        void OpenSite();
        void ShowHideApp(bool isForceShow);
    }
}