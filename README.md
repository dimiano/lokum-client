### Lokum Client ###
WPF project
Simple Lokum client.

**Functions**: every configured time interval fetches data from lokum developer site to get the latest information about flats available.
If any - shows notification message and proposes to navigate to the site.
Also Tray Notification is available after hidding main window.
This client is supposed to stay in tray and periodically check the Lokum side for news and updates.

This project is currently in setup mode and available to all developers.